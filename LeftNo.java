import java.util.Scanner;

public class LeftNo {
	
	
	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println("How many rows do you want?");
			
			int rows=sc.nextInt();
			
			for (int i = 1; i <= rows; i++) {
				for (int j = 1; j <= i; j++) {
					System.out.print(j + " ");
				}
				
				System.out.println();
			}
		}

	}
		
}
