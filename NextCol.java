public class NextCol {
    public static void main(String[] args) {
        
    	int rows = 5;       
    			
        for (int i = 1; i <= rows; i++) {
        
            for (int j = 1; j <= i; j++) {
            
                if(j%2 == 0) {
                
                    System.out.print(j);
                }
                
                
                else {
                
                    System.out.print(i);
                }
            }
              
            System.out.println();
        }
    }
}
