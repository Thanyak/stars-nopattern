
public class NoPattern2 {
	
	static int row = 7;

	public static void main(String[] args) {
		for (int r = 1; r <= row; r++) {

			for (int s = 1; s <= row-r; s++) {
				System.out.print("1  ");
			}
			
			for (int sp = 1; sp <= r; sp++) {
				System.out.print(r + "  ");
			}
			System.out.println();
		}
	}
}

//1  1  1  1  1  1  1  
//1  1  1  1  1  2  2  
//1  1  1  1  3  3  3  
//1  1  1  4  4  4  4  
//1  1  5  5  5  5  5  
//1  6  6  6  6  6  6  
//7  7  7  7  7  7  7  