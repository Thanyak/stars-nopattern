
public class NoPattern9 {
	public static void main(String[] args) {
		
		int rows = 7;
		   
		for (int i = 1; i <= rows; i++) {
	
			for (int j = 1; j <= i; j++) {
	   
				System.out.print(" ");
			}	
			for (int j = i; j<= rows; j++) {
					   
				System.out.print(j);

			}
			System.out.println();
		}
		int row = 7;
		   
		for (int i = 1; i <= row-1; i++) {
	
			for (int j = row-1; j >=i; j--) {
	   
				System.out.print(" ");
			}	
			for (int j = row-i; j<= row; j++) {
					   
				System.out.print(j);

			}
			System.out.println();
		}
	}
}

//1234567
// 234567
//  34567
//   4567
//    567
//     67
//      7
//     67
//    567
//   4567
//  34567
// 234567
//1234567
