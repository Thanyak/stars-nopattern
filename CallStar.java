import java.util.Scanner;

public class CallStar {

	// static int num = 5;

	public static void main(String[] args) {
		try (Scanner scan = new Scanner(System.in)) {
			System.out.println("Enter the row no:");
			int row = scan.nextInt();

			for (int i = 1; i <= row; i++) {

				for (int j = 1; j <= i; j++) {
					System.out.print(" * ");
				}
				System.out.println();
			}
		}
	}


}
