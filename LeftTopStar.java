
public class LeftTopStar {

	static int row = 5;

	public static void main(String[] args) {
		for (int r = 1; r <= row; r++) {

			for (int s = 0; s <= row-r; s++) {
				System.out.print(" * ");
			}
			
			for (int sp = 1; sp <= r; sp++) {
				System.out.print("  ");
			}
			System.out.println();
		}
	}

}

//* * * * *
//* * * *
//* * *
//* *
//*